package;

import flixel.util.FlxSave;

/**
 * Handy, pre-built Registry class that can be used to store
 * references to objects and other things for quick-access. Feel
 * free to simply ignore it or change it in any way you like.
 */
class Reg
{
	/**
	 * Generic levels Array that can be used for cross-state stuff.
	 * Example usage: Storing the levels of a platformer.
	 */
	public static var levels:Array<Dynamic> = [];
	/**
	 * Generic level variable that can be used for cross-state stuff.
	 * Example usage: Storing the current level number.
	 */
	public static var level:Int = 0;
	/**
	 * Generic scores Array that can be used for cross-state stuff.
	 * Example usage: Storing the scores for level.
	 */
	public static var scores:Array<Int> = [0, 0, 0, 0];

	public static var deaths:Array<Int> = [0, 0, 0, 0];
	/**
	 * Generic score variable that can be used for cross-state stuff.
	 * Example usage: Storing the current score.
	 */
	public static var score:Int = 0;
	/**
	 * Generic bucket for storing different FlxSaves.
	 * Especially useful for setting up multiple save slots.
	 */
	public static var saves:Array<FlxSave> = [];

	public static var selectedArena:String = "";

	public static var PATH_TILESHEETS:String = "assets/images/tiled/";
	public static var PATH_IMAGES:String = "assets/images/";
	public static var PATH_IMAGES_EXT:String = ".png";
	public static var TILED_PATH:String = "assets/data/";
	public static var TILED_EXT:String = ".tmx";
	public static var ENTITIES_PATH:String = "bluemonkeys.entities.";
	public static var SCREENS_PATH:String = "bluemonkeys.screens.";
	public static var TIME_LIMIT:Int = 150;
	public static var TIME_WARNING:Int = 10;
	public static var ARENAS_LIST:Array<String> = ["arena", "open_wild"];
	public static var PAUSE_EVENT:String = "GAMEPLAY_PAUSE_EVENT";

	// options list
	public static var alternativeForceModel:Bool = false;

}