package bluemonkeys.screens;

import flixel.addons.ui.FlxUIState;

/**
* This screen should be used to exit the game. Any uninitializations should be done here.
* It also nicely transitions to black before exiting.
**/
class ExitScreen extends FlxUIState{
    override public function create() {
        Sys.exit(0);
    }
}
