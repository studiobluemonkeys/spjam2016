package bluemonkeys.screens;

import bluemonkeys.screens.utils.Callback;
import hscript.Expr;
import haxe.xml.Fast;
import flixel.addons.ui.FlxUI;
import flixel.addons.ui.FlxUIState;
import flixel.addons.ui.interfaces.IEventGetter;
import flixel.addons.ui.interfaces.IFireTongue;
import flixel.FlxG;
import flixel.tweens.FlxTween;
import flixel.tweens.FlxEase;



/**
* This class adds functionalities to flixel-ui screens (it interprets some scripts
* directly from xml files), making menu classes reusable.
*
* The differences to default flixel-ui files are a new <init_script> tag and a new
* param type (script)
*
* <init_script>: Should be child of <data> element. It has one attribute `value`
* that will be the script that will execute on screen load (on `finish_load` event).
*
* <param type="script">: Its `value` param should have a hscript that contains
* an array of `Callback` instances. The value used on `name` argument will be used
* to compare to current event name of the widget. If it matches (all or part of it),
* it will be executed. Example:
*
* ```
* <data>
*     <button use_def="button_green" center_x="true" x="0" y="643" name="testButton" label="Test">
*         <param type="script" value="
*         [
*             new Callback('click', function() {
*                 trace('the button ${this.name} was clicked!');
*             })
*         ]
*         " />
*     </button>
* </data>
* ```
*
* Exported variables:
* `this`: the `IFlxUIWidget` being processed
* `screen`: the instance of current screen
*
* Exported functions:
* `SwitchScreen`: FlxG.switchState (used to change to another screen)
*
* Exported classes:
* `FlxTween` (useful to animate elements on screen)
* `FlxEase` (to use with tween functions)
* `FlxG` (general Flixel functions)
* `Callback` (used to create callbacks to trigger on UI events)
*
* Screen classes should be on 'bluemonkeys.screens' package
*
* It's always possible to export new variables to UI hscript. Just extend this class
* and set new values on `interp.variables`.
*/

class MenuScreen extends FlxUIState {

    private var interp:hscript.Interp;
    private var init_script:Expr;

    public function new(xmlToLoad:String) {
        if (xmlToLoad == null || xmlToLoad.length == 0) {
            throw 'No valid XML to load.';
        }
        super();

        // Interp Initialization
        interp = new hscript.Interp();

        // Useful general functions/classes
        interp.variables["SwitchScreen"] = function (screenName:String, ?screenParams:Array<Dynamic>) {
            if (screenParams == null) screenParams = [];
            var screenClass:Class<Dynamic> = Type.resolveClass('${Reg.SCREENS_PATH}${screenName}');
            if (screenClass == null) {
                FlxG.log.error('Screen class ${screenName} not found.');
                return;
            }
            FlxG.switchState(Type.createInstance(screenClass, screenParams));
        }
        interp.variables["FlxTween"] = FlxTween;
        interp.variables["FlxEase"] = FlxEase;
        interp.variables["FlxG"] = FlxG;
        interp.variables["Callback"] = Callback;
        interp.variables["Reg"] = Reg;

        // Expose screen instance to script
        interp.variables["screen"] = this;

        _xml_id = xmlToLoad;
    }

    // Only difference from this to parent is that it creates an UIElement instead of FlxUI
    override private function createUI(data:Fast = null, ptr:IEventGetter = null, superIndex_:FlxUI = null, tongue_:IFireTongue = null, liveFilePath_:String=""):FlxUI
    {
        var flxui = new UIElement(data, ptr, superIndex_, tongue_, liveFilePath_, _ui_vars);
        _cleanupUIVars();	//clear out temporary _ui_vars variable if it was set
        return flxui;
    }

    /**
     * Processes a FlxUI event. If there's a callback script associated with this event, it will be triggered.
     *
     * @param	name	string identifier of the event -- each IFlxUIWidget has a set of string constants
     * @param	sender	the IFlxUIWidget that sent this event
     * @param	data	non-array data (boolean for a checkbox, string for a radiogroup, etc)
     * @param	params	(optional) user-specified array of arbitrary data
     */
    override public function getEvent(name:String, sender:Dynamic, data:Dynamic, ?params:Array<Dynamic>):Void {
        if (name == "finish_load" && init_script != null) {
            interp.variables["this"] = this;
            interp.execute(init_script);
            return;
        }

        if (params != null) {
            for (param in params) {
                // filter only script params
                if (Std.is(param, Expr)) {
                    // Expose UI element to script
                    interp.variables["this"] = sender;
                    // Expose UI data to script
                    if (data != null) interp.variables["data"] = data;
                    // Execute callback that matches event name
                    var ret:Array<Callback> = cast interp.execute(param);
                    for (cb in ret) {
                        if (name.indexOf(cb.name) != -1){
                            cb.func();
                        }
                    }
                }
            }
        }
    }

    /**
    * Handles unknown flixel-ui elements so new XML elements can be parsed (even to create new UI elements).
    * Here, only <init_script> tag is being considered.
    **/
    override public function getRequest(id:String, sender:Dynamic, data:Dynamic, ?params:Array<Dynamic>):Dynamic {
        // it's possible to handle other XML elements here, and even create new UI elements
        var xml:Xml = cast data.x;
        switch(xml.nodeName){
            case "init_script":
                var parser = new hscript.Parser();
                var scriptData = xml.get("value");
                if (scriptData.length == 0) scriptData = xml.firstChild().nodeValue;
                init_script = parser.parseString(scriptData);
        }
        return null;
    }

}
