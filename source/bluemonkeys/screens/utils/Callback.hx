package bluemonkeys.screens.utils;
/**
* This class is used to create callbacks on hscripts. Example of how to use it on
* MenuScreen class documentation.
**/
class Callback {
    public var name:String;
    public var func:Void->Void;
    public function new(name:String, func:Void->Void) {
        this.name = name;
        this.func = func;
    }
}