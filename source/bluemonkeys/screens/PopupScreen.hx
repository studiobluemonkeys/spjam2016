package bluemonkeys.screens;

import haxe.xml.Fast;
import flixel.addons.ui.interfaces.IFireTongue;
import flixel.addons.ui.FlxUI;
import flixel.addons.ui.interfaces.IEventGetter;
import bluemonkeys.screens.utils.Callback;
import flixel.tweens.FlxEase;
import flixel.tweens.FlxTween;
import flixel.FlxG;
import hscript.Expr;
import flixel.addons.ui.FlxUIPopup;

class PopupScreen extends FlxUIPopup {
    private var interp:hscript.Interp;
    private var init_script:Expr;

    public function new(xmlToLoad:String) {
        if (xmlToLoad == null || xmlToLoad.length == 0) {
            throw 'No valid XML to load.';
        }
        super();

        // Interp Initialization
        interp = new hscript.Interp();

        // Useful general functions/classes
        interp.variables["SwitchScreen"] = function (screenName:String, ?screenParams:Array<Dynamic>) {
            if (screenParams == null) screenParams = [];
            var screenClass:Class<Dynamic> = Type.resolveClass('${Reg.SCREENS_PATH}${screenName}');
            if (screenClass == null) {
                FlxG.log.error('Screen class ${screenName} not found.');
                return;
            }
            FlxG.switchState(Type.createInstance(screenClass, screenParams));
        }
        interp.variables["FlxTween"] = FlxTween;
        interp.variables["FlxEase"] = FlxEase;
        interp.variables["FlxG"] = FlxG;
        interp.variables["Callback"] = Callback;
        interp.variables["Reg"] = Reg;

        // Expose screen instance to script
        interp.variables["screen"] = this;

        _xml_id = xmlToLoad;
    }

    // Only difference from this to parent is that it creates an UIElement instead of FlxUI
    override private function createUI(data:Fast = null, ptr:IEventGetter = null, superIndex_:FlxUI = null, tongue_:IFireTongue = null, liveFilePath_:String=""):FlxUI
    {
        return new UIElement(data, ptr, superIndex_, tongue_, liveFilePath_);
    }

    override public function getEvent(name:String, sender:Dynamic, data:Dynamic, ?params:Array<Dynamic>):Void {
        if (name == "finish_load" && init_script != null) {
            interp.variables["this"] = this;
            interp.execute(init_script);
            return;
        }

        if (params != null) {
            for (param in params) {
                // filter only script params
                if (Std.is(param, Expr)) {
                    // Expose UI element to script
                    interp.variables["this"] = sender;
                    // Expose UI data to script
                    if (data != null) interp.variables["data"] = data;
                    // Execute callback that matches event name
                    var ret:Array<Callback> = cast interp.execute(param);
                    for (cb in ret) {
                        if (name.indexOf(cb.name) != -1){
                            cb.func();
                        }
                    }
                }
            }
        }
    }

    /**
    * Handles unknown flixel-ui elements so new XML elements can be parsed (even to create new UI elements).
    * Here, only <init_script> tag is being considered.
    **/
    override public function getRequest(id:String, sender:Dynamic, data:Dynamic, ?params:Array<Dynamic>):Dynamic {
        // it's possible to handle other XML elements here, and even create new UI elements
        var xml:Xml = cast data.x;
        switch(xml.nodeName){
            case "init_script":
                var parser = new hscript.Parser();
                var scriptData = xml.get("value");
                if (scriptData.length == 0) scriptData = xml.firstChild().nodeValue;
                init_script = parser.parseString(scriptData);
        }
        return null;
    }
}
