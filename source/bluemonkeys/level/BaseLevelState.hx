package bluemonkeys.level;

import bluemonkeys.screens.MenuScreen;
import haxe.Timer;
import bluemonkeys.events.PauseEvent;
import flixel.tweens.FlxTween;
import bluemonkeys.screens.PopupScreen;
import flixel.FlxG;
import flixel.FlxState;
import flixel.group.FlxGroup;
import flixel.system.scaleModes.*;
import flixel.FlxObject;
import flixel.input.gamepad.FlxGamepad;
import flixel.math.FlxPoint;
import flixel.FlxSprite;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.math.FlxMath;
import flixel.math.FlxPoint;
import bluemonkeys.entities.*;
import flixel.util.FlxTimer;
import flixel.text.FlxText;
import flixel.util.FlxColor;
import flixel.ui.FlxBar;

import bluemonkeys.lib.TiledMapLoader;
import flixel.addons.transition.FlxTransitionableState;



/**
 * A FlxState which can be used for the actual gameplay.
 */
class BaseLevelState extends FlxTransitionableState
{
	private inline static var TIME_UP_TIMEOUT = 2;
	private var entities:Map<String, Entity>;
	private var levelName:String;

	var players:Array<Player>;
	var activePlayers:Int;

    private var scrap:Scrap;
    private var movementDirection:FlxPoint = new FlxPoint();

	var matchTimer = new FlxTimer();
	public var remainingTime:Int = Reg.TIME_LIMIT;
	var timerLabel:FlxText;
	private var paused = false;
	public function new(levelName:String) {
		super();
		if (levelName == null) throw "Null map cannot be loaded.";
		this.levelName = levelName;

		this.entities = new Map<String, Entity>();

		bluemonkeys.entities.BaseSpawner.resetGroups();
	}
	/**
	 * Function that is called up when to state is created to set it up.
	 */
	override public function create():Void
	{
		super.create();
		players = [new Player(160, 96, 0), new Player(864, 96, 1), new Player(160, 672, 2), new Player(864, 672, 3)];


		#if debug
		FlxG.debugger.visible = true;
		FlxG.debugger.drawDebug = true;
		#end


		FlxG.mouse.visible = false;

		var tileMapLoader:TiledMapLoader = new TiledMapLoader(Reg.TILED_PATH + levelName + Reg.TILED_EXT, objectFactory);

		for (tileLayer in tileMapLoader.tileLayers) {
			this.add(tileLayer);
		}

		for(entity in entities){
			entity.afterSceneLoad();
		}

		// update physics bounds
		tileMapLoader.updateWorldBounds();

		// Background
		bgColor = 0xffacbcd7;

		activePlayers = cast Math.min(inputAmount(), 4);

		var i = 0;
		while (i < activePlayers){
			add(players[i]);
			add(players[i].healthBar);
			i++;
		}

		// Timer title
		var timerTitle = new FlxText(0, 15, "TEMPO", 32);
		timerTitle.x = FlxG.width/2 - timerTitle.width/2;
		timerTitle.borderColor = FlxColor.BLACK;
		timerTitle.borderSize = 3;
		timerTitle.borderStyle = OUTLINE;
		add(timerTitle);

		timerLabel = new FlxText(0, 60, Std.string(Reg.TIME_LIMIT), 32);
		timerLabel.x = FlxG.width/2 - timerLabel.width/2;
		timerLabel.borderColor = FlxColor.BLACK;
		timerLabel.borderSize = 3;
		timerLabel.borderStyle = OUTLINE;
		add(timerLabel);

		// set up timer
		matchTimer.start(1, timerCallback, Reg.TIME_LIMIT + 1);

		FlxG.sound.playMusic("assets/music/arena.ogg", 0.8, true);

		FlxG.stage.addEventListener(Reg.PAUSE_EVENT, gamePaused, false, 0, true);
	}

	private function gamePaused(event:PauseEvent) {
		paused = event.paused;
		matchTimer.active = !event.paused;
	}
	private function timerCallback(timer:FlxTimer) {
		if (remainingTime == 0) {
			// time up
			timeUp();
			return;
		}
		// visually warns when time is ending
		if (remainingTime <= Reg.TIME_WARNING) {
			FlxTween.color(timerLabel, 0.5, FlxColor.WHITE, FlxColor.RED);
		}
		remainingTime--;
		timerLabel.text = Std.string(remainingTime);
	}

	// called when time is zero
	private function timeUp() {
		// pause everything game related
		FlxG.stage.dispatchEvent(new PauseEvent(true));

		var timeUpText = new FlxText(0, 60, "TIME UP", 128);
		timeUpText.x = FlxG.width/2 - timeUpText.width/2;
		timeUpText.borderColor = FlxColor.BLACK;
		timeUpText.borderSize = 3;
		timeUpText.borderStyle = OUTLINE;
		add(timeUpText);
		// change screen after 1 second
		(new FlxTimer()).start(TIME_UP_TIMEOUT, function(t:FlxTimer) {FlxG.switchState(new MenuScreen('game_over'));});
	}
	private function objectFactory(defProps:Map<String, String>, custProps:Map<String, String>, layerProps:Map<String, String>){
		var objType:Class<Dynamic> = Type.resolveClass(Reg.ENTITIES_PATH + defProps["type"]);
		if (objType == null) {
			FlxG.log.warn('type ${defProps["type"]} not found (object name "${defProps["name"]}").');
			return null;
		}
		FlxG.log.notice('type ${defProps["type"]} found (object name "${defProps["name"]}").');
		var obj:Entity = Type.createInstance(objType, []);
		obj.load(defProps, custProps, layerProps);
		if(defProps["name"] != null) {
			FlxG.log.notice('adding entity named ${defProps["name"]}.');
			entities.set(defProps["name"], obj);
		}
		return obj;
	}

	/**
	 * Function that is called when this state is destroyed - you might want to
	 * consider setting all objects this state uses to null to help garbage collection.
	 */
	override public function destroy():Void
	{
		super.destroy();
	}

	/**
	 * Function that is called once every frame.
	 */
	override public function update(elapsed:Float):Void
	{
		super.update(elapsed);

		var i = 0;
		while (i < activePlayers){
			//Do not call spawnmanager after FlxG.collide,
			//our physics manipulation gets messed up
			SpawnManager.collideWithSpawned(players[i]);
			handlePlayerInput(i);
			i++;
		}

		FlxG.collide();

	}

	private function inputAmount() {
		return FlxG.gamepads.numActiveGamepads + 1;
	}

	private function handlePlayerInput(index:Int) {
		movementDirection.set();
		var polarity:bluemonkeys.Player.ForceType = NONE;
		if (!paused) {
			if (index == 0) {
				// keyboard input
				if (FlxG.keys.anyPressed([LEFT])) {
					movementDirection.x = -1;
				} else if (FlxG.keys.anyPressed([RIGHT])) {
					movementDirection.x = 1;
				}

				if (FlxG.keys.anyPressed([UP])) {
					movementDirection.y = -1;
				} else if (FlxG.keys.anyPressed([DOWN])) {
					movementDirection.y = 1;
				}

				if (FlxG.keys.anyPressed([SHIFT])) {
					polarity = ATTRACT;
				} else if (FlxG.keys.anyPressed([SPACE])) {
					polarity = REPULSE;
				} else if (FlxG.keys.anyJustReleased([SHIFT, SPACE])) {
					polarity = NONE;
				}

				// pause game when esc is pressed (might be good to pause with joystick)
				if (FlxG.keys.anyJustPressed([ESCAPE])) {
					FlxG.stage.dispatchEvent(new PauseEvent(true));

					var pauseState = new PopupScreen('pause_popup');
					pauseState.closeCallback = function() FlxG.stage.dispatchEvent(new PauseEvent(false));

					openSubState(pauseState);
				}

			} else {
				// gamepad input
				var gamepad = FlxG.gamepads.getByID(index - 1);
				if (gamepad != null && gamepad.connected) {
					movementDirection.set(gamepad.getAxis(0), gamepad.getAxis(1));
					polarity = NONE;
					if (gamepad.checkStatus(0,1)) {
						polarity = ATTRACT;
					} else if (gamepad.checkStatus(1,1)) {
						polarity = REPULSE;
					}
				}
			}
		}
		players[index].setMovementDirection(movementDirection);
        players[index].setPolarity(polarity);
	}

	public function getEntity(name:String):Entity {
		return entities.get(name);
	}
}
