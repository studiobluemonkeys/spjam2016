package bluemonkeys.level;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.util.FlxTimer;

import bluemonkeys.entities.Entity;
import bluemonkeys.entities.ISpawner;

/**
* Has spawners so we can access their children for collision
*/
class SpawnManager {
	private static var spawners:Array<ISpawner> = [];

    public static function addSpawner(spawner:ISpawner):Void {
        spawners.push(spawner);
        FlxG.log.warn('spawner ${spawner} add to spawners in Manager.');
    }

    public static function collideWithSpawned(player:Player):Void {
    	for (spawner in spawners) {            
    		spawner.collideWithPlayer(player);
    	}
    }


}
