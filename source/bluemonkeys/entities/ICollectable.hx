package bluemonkeys.entities;

import bluemonkeys.Player;

interface ICollectable {
	public function useWith(player:Player):Void;
}