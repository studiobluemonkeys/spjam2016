package bluemonkeys.entities;

import flixel.util.FlxDestroyUtil;

typedef TiledProperties = Map<String, String>;

interface Entity extends IFlxDestroyable {
	public function load(defaultProperties:TiledProperties, customProperties:TiledProperties, layerProperties:TiledProperties):Void;
	public function afterSceneLoad():Void;
}