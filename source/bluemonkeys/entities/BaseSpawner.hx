package bluemonkeys.entities;

import bluemonkeys.events.PauseEvent;
import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.math.FlxPoint;
import flixel.util.FlxTimer;
import flixel.group.FlxGroup;

import bluemonkeys.entities.Entity;
import bluemonkeys.level.SpawnManager;
import bluemonkeys.entities.ICollectable;

/**
* BaseSpawner is the default implementation for spawners of collectables
*/
class BaseSpawner extends FlxObject implements Entity implements ISpawner {
	private var LOOP_FOREVER:Int = 0;

	private var spawnTimer:FlxTimer = new FlxTimer();
	public var spawnWaitUntilNextMin:Float = 2;
	public var spawnLimitAtOnce:Int = 1;

	private var spawneds:Array<ICollectable> = [];

	private static var spawnedsGroup:FlxGroup = new FlxGroup();
	private static var isSpawnedsGroupAdded:Bool = false;

    public static function resetGroups():Void {
        spawnedsGroup = new FlxGroup();
        isSpawnedsGroupAdded = false;
    }

    public function getImageAdress():String {
        throw "NotImplementedException";
    }

    public function getSize():FlxPoint {
        throw "NotImplementedException";   
    }

	public function new(x:Float, y:Float) {
		super(x, y);
        solid = false;
        FlxG.stage.addEventListener(Reg.PAUSE_EVENT, gamePaused, false, 0, true);
	}
    private function gamePaused(event:PauseEvent) {
        spawnTimer.active = !event.paused;
    }
    public function load(defaultProperties:TiledProperties,
		customProperties:TiledProperties, 
		layerProperties:TiledProperties):Void
    {
        this.setPosition(Std.parseInt(defaultProperties["x"]), 
        				 Std.parseInt(defaultProperties["y"]));

        var waitTimeProp   = customProperties["spawnWaitUntilNextMin"];
        var spawnLimitProp = customProperties["spawnLimitAtOnce"];

        if(waitTimeProp != null) {
            spawnWaitUntilNextMin = Std.parseFloat(waitTimeProp);
        }
        if(spawnLimitProp != null) {
            spawnLimitAtOnce = Std.parseInt(spawnLimitProp);
        }

        solid = false;

        SpawnManager.addSpawner(this);
    }

    public function afterSceneLoad() {
		if(!isSpawnedsGroupAdded) {
			FlxG.state.add(spawnedsGroup);
			isSpawnedsGroupAdded = true;
		}

    	spawnTimer.start(spawnWaitUntilNextMin, function(timer:FlxTimer) {
    		if(spawneds.length < spawnLimitAtOnce ) {
    			spawn();
    		}
    		else {
				respawnDeadOnes();
    		}
    	}, LOOP_FOREVER);
    }

    public function generateSpawnable():ICollectable {
        throw "NotImplementedException";
    }

    public function spawn():Void {
    	var spawnable = generateSpawnable();
    	spawneds.push(spawnable);
        spawnedsGroup.add(cast spawnable);
    }

    public function respawnDeadOnes():Void {
    	for (spawned in spawneds) {
			var collectable:FlxSprite = cast spawned;
            if(!collectable.alive) {
                collectable.reset(this.x+32-collectable.width/2, 
                                this.y+32-collectable.height/2);
                return ;
            }            
    	}
    }

    public function collideWithPlayer(player:Player) {		
		FlxG.collide(player, spawnedsGroup, playerHitSpawned);
    }

    public function playerHitSpawned(player:Player, collectable:ICollectable):Void {
		FlxG.watch.addQuick("Collision",'collectable ${collectable} collided with $player.');
        collectable.useWith(player);        
    }
}
