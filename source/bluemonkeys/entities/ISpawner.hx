package bluemonkeys.entities;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.util.FlxTimer;

import bluemonkeys.Player;

/**
* 
*/
interface ISpawner {
    public function collideWithPlayer(player:Player):Void;
}
