package bluemonkeys.entities;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.util.FlxTimer;
import flixel.group.FlxGroup;
import flixel.math.FlxPoint;

import bluemonkeys.entities.Entity;
import bluemonkeys.level.SpawnManager;
import bluemonkeys.entities.ICollectable;
import bluemonkeys.entities.HealthCollectable;

/**
* HealthSpawner spawns collectables that adds HP to the player
*/
class HealthSpawner extends BaseSpawner {
    
    public override function getImageAdress():String {        
        return "assets/images/bonus64x.png";
    }

    public override function getSize():FlxPoint {
        return new FlxPoint(64, 64);
    }

    public override function generateSpawnable():ICollectable {        
        return new HealthCollectable(this.x+32, this.y+32);
    }
}