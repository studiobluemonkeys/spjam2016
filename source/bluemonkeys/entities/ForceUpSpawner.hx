package bluemonkeys.entities;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.util.FlxTimer;
import flixel.group.FlxGroup;
import flixel.math.FlxPoint;

import bluemonkeys.entities.Entity;
import bluemonkeys.level.SpawnManager;
import bluemonkeys.entities.ICollectable;
import bluemonkeys.entities.ForceUpCollectable;

/**
* ForceUpSpawner spawns collectables that adds force to the player
*/
class ForceUpSpawner extends BaseSpawner {
    
    public override function getImageAdress():String {        
        return "assets/images/bonus64x.png";
    }

    public override function getSize():FlxPoint {
        return new FlxPoint(1, 1);
    }

    public override function generateSpawnable():ICollectable {        
        return new ForceUpCollectable(this.x+32, this.y+32);
    }
}