package bluemonkeys.entities;

import flixel.FlxSprite;
import flixel.util.FlxTimer;
import bluemonkeys.entities.ICollectable;
import bluemonkeys.lib.SingleSfxPlayer;

/**
* Scrap is everything that would hurt the player someway.
* configure based on Spawner: sprite and damage power
*/
class Scrap extends AttractableObject implements ICollectable{
    private var power:Float = 20;
    private var sfxPlayer:SingleSfxPlayer;

    public function new(x:Float, y:Float)
    {
        super(x, y, "assets/images/pedra_001.png");
        sfxPlayer = new SingleSfxPlayer("assets/sounds/baditem.wav");

        this.x -= this.width/2;
        this.y -= this.height/2;
    }

    public function getPower():Float {
        return power;
    }

    public function useWith(player:Player) {
        sfxPlayer.play();

        player.doDamage(getPower());
        kill();
    }
}
