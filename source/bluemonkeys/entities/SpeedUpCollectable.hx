package bluemonkeys.entities;

import flixel.FlxSprite;
import flixel.util.FlxTimer;
import bluemonkeys.entities.ICollectable;
import bluemonkeys.Player;
import bluemonkeys.lib.SingleSfxPlayer;

/**
* SpeedUpCollectable gives speed to the player
*/
class SpeedUpCollectable extends AttractableObject implements ICollectable {
    private var speedIncrease:Float = 2.0;
    private var durationInSeconds:Float = 3;
    private var sfxPlayer:SingleSfxPlayer;

    public function getSpeedIncrease():Float {
        return speedIncrease;
    }

    public function new(x:Float, y:Float)
    {
        super(x, y, "assets/images/pocao_vel_001.png");
        sfxPlayer = new SingleSfxPlayer("assets/sounds/lifeitem.wav");

        this.x -= this.width/2;
        this.y -= this.height/2;
    }

    public function useWith(player:Player) {        
        sfxPlayer.play();
        player.applySpeedModifier(speedIncrease, durationInSeconds);
        kill();
    }
}
