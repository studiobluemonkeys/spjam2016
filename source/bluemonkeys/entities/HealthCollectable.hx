package bluemonkeys.entities;

import flixel.FlxSprite;
import flixel.util.FlxTimer;
import bluemonkeys.entities.ICollectable;
import bluemonkeys.Player;
import bluemonkeys.lib.SingleSfxPlayer;

/**
* HealthCollectable gives health to the player
*/
class HealthCollectable extends AttractableObject implements ICollectable {
    private var restorePoints:Int = 5;
    private var sfxPlayer:SingleSfxPlayer;

    public function getPoints():Int {
        return restorePoints;
    }

    public function new(x:Float, y:Float)
    {
        super(x, y, "assets/images/pocao_vid_001.png");
        sfxPlayer = new SingleSfxPlayer("assets/sounds/lifeitem.wav");

        this.x -= this.width/2;
        this.y -= this.height/2;
    }

    public function useWith(player:Player) {

        sfxPlayer.play();

        player.applyHealthKit(restorePoints);
        kill();
    }
}
