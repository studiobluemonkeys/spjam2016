package bluemonkeys.entities;

import flixel.FlxSprite;
import flixel.util.FlxTimer;
import bluemonkeys.entities.ICollectable;
import bluemonkeys.Player;
import bluemonkeys.lib.SingleSfxPlayer;

/**
* Upon collect player will receive points
*/
class ScoreBonus extends AttractableObject implements ICollectable {
    private var points:Int = 5;
    private var sfxPlayer:SingleSfxPlayer;

    public function getPoints():Int {
        return points;
    }

    public function new(x:Float, y:Float)
    {
        super(x, y, "assets/images/moeda_001.png");
        sfxPlayer = new SingleSfxPlayer("assets/sounds/collectable.wav");
        this.x -= this.width/2;
        this.y -= this.height/2;
    }

    public function useWith(player:Player) {        
        sfxPlayer.play();
        player.addScore(points);
        kill();
    }
}
