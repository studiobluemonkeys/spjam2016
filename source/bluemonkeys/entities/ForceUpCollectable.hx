package bluemonkeys.entities;

import flixel.FlxSprite;
import flixel.util.FlxTimer;
import bluemonkeys.entities.ICollectable;
import bluemonkeys.Player;
import bluemonkeys.lib.SingleSfxPlayer;

/**
* ForceUpCollectable gives force to the player
*/
class ForceUpCollectable extends AttractableObject implements ICollectable {
    private var forceIncrease:Float = 2.5;
    private var durationInSeconds:Float = 3;
    private var sfxPlayer:SingleSfxPlayer;

    public function new(x:Float, y:Float)
    {
        super(x, y, "assets/images/pocao_for_001.png");
        sfxPlayer = new SingleSfxPlayer("assets/sounds/lifeitem.wav");

        this.x -= this.width/2;
        this.y -= this.height/2;
    }

    public function useWith(player:Player) {        
        sfxPlayer.play();

        player.applyForceModifier(forceIncrease, durationInSeconds);
        kill();
    }
}
