package bluemonkeys.entities;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.util.FlxTimer;
import flixel.group.FlxGroup;
import flixel.math.FlxPoint;

import bluemonkeys.entities.Entity;
import bluemonkeys.level.SpawnManager;

/**
* ScrapSpawner spawns scraps
*/
class ScrapSpawner extends bluemonkeys.entities.BaseSpawner implements ISpawner {

    public override function getImageAdress():String {        
        return "";
    }

    public override function getSize():FlxPoint {
        return new FlxPoint(64, 64);
    }

    public override function generateSpawnable():ICollectable {        
        return new bluemonkeys.entities.Scrap(this.x+32, this.y+32);
    }
}
