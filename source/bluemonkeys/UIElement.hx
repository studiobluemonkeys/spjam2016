package bluemonkeys;

import flixel.addons.ui.BorderDef;
import flixel.addons.ui.FlxInputText;
import flixel.addons.ui.FlxUI;
import flixel.addons.ui.FlxUI9SliceSprite;
import flixel.addons.ui.FlxUIAssets;
import flixel.addons.ui.FlxUIButton;
import flixel.addons.ui.FlxUICheckBox;
import flixel.addons.ui.FlxUIInputText;
import flixel.addons.ui.FlxUINumericStepper;
import flixel.addons.ui.FlxUIRadioGroup;
import flixel.addons.ui.FlxUISprite;
import flixel.addons.ui.FlxUISpriteButton;
import flixel.addons.ui.FlxUIText;
import flixel.addons.ui.FlxUITypedButton;
import flixel.addons.ui.FontDef;
import flixel.addons.ui.interfaces.*;
import flixel.addons.ui.U;
import flixel.FlxSprite;
import flixel.group.FlxSpriteGroup;
import flixel.math.FlxPoint;
import flixel.system.FlxAssets;  // used in one function, do not delete!
import flixel.text.FlxText;
import flixel.util.FlxStringUtil;
import openfl.display.BitmapData;
import openfl.errors.Error;

import haxe.xml.Fast;

using StringTools;

/**
* This class is the same as FlxUI class except that it also accepts hscript as param type.
*/
class UIElement extends FlxUI{
    private static var parser = new hscript.Parser();

    // Only difference to the base class is the "script" parameter type
    private static inline function getParams(data:Fast):Array<Dynamic>{
        var params:Array<Dynamic> = null;

        if (data.hasNode.param) {
            params = new Array<Dynamic>();
            for (param in data.nodes.param) {
                if(param.has.type){
                    var type:String = param.att.type;
                    type = type.toLowerCase();
                    var valueStr:String;
                    if (param.has.value)
                        valueStr = param.att.value;
                    else
                        valueStr = param.innerData;
                    if (valueStr.length == 0) continue;
                    valueStr = valueStr.trim();
                    var value:Dynamic = valueStr;
                    switch(type) {
                        case "string": value = new String(valueStr);
                        case "int": value = Std.parseInt(valueStr);
                        case "float": value = Std.parseFloat(valueStr);
                        case "color", "hex": value = U.parseHex(valueStr, true);
                        case "bool", "boolean":
                            var str:String = valueStr.toLowerCase();
                            value = (str == "true" || str == "1");
                        case "hscript", "script", "code":
                            #if hscript
                            value = parser.parseString(valueStr);
                            #else
                            trace('Compiled without hscript support.');
                            #end
                    }
                    //Add value to the array
                    params.push(value);
                }
            }
        }
        return params;
    }

    // The functions below are duplicated from base class only to work using above function. All of its functionalities
    // are preserved.

    @:access(flixel.addons.ui.FlxUITypedButton)
    override private function _loadButton(data:Fast, setCallback:Bool = true, isToggle:Bool = false, load_code:String = ""):IFlxUIWidget
    {
        var src:String = "";
        var fb:IFlxUIButton = null;

        var resize_ratio:Float = U.xml_f(data.x, "resize_ratio", -1);
        var resize_point:FlxPoint = _loadCompass(data, "resize_point");
        var resize_label:Bool = U.xml_bool(data.x, "resize_label", false);

        var label:String = U.xml_str(data.x, "label");

        var sprite:FlxUISprite = null;
        var toggleSprite:FlxUISprite = null;
        if (data.hasNode.sprite)
        {
            for (spriteNode in data.nodes.sprite)
            {
                var forToggle:Bool = isToggle && U.xml_bool(spriteNode.x, "toggle");
                if (forToggle)
                {
                    toggleSprite = cast _loadThing("sprite", spriteNode);
                }
                else
                {
                    sprite = cast _loadThing("sprite", spriteNode);
                }
            }
        }

        var context:String = U.xml_str(data.x, "context", true, "ui");
        var code:String = U.xml_str(data.x, "code", true, "");

        label = getText(label,context,true,code);

        var W:Int = Std.int(_loadWidth(data, 0, "width"));
        var H:Int = Std.int(_loadHeight(data, 0, "height"));

        var params:Array<Dynamic> = getParams(data);

        if (sprite == null)
        {
            var useDefaultGraphic = (data.hasNode.graphic == false);
            fb = new FlxUIButton(0, 0, label, null, useDefaultGraphic);
            var fuib:FlxUIButton = cast fb;
            fuib._autoCleanup = false;
        }
        else
        {
            var tempGroup:FlxSpriteGroup = null;
            if (label != "")
            {
                //We have a Sprite AND a Label, so we package it up in a group

                var labelTxt = new FlxUIText(0, 0, 80, label, 8);

                labelTxt.setFormat(null, 8, 0x333333, "center");

                tempGroup = new FlxSpriteGroup();

                tempGroup.add(sprite);
                tempGroup.add(labelTxt);

                fb = new FlxUISpriteButton(0, 0, tempGroup);
            }
            else
            {
                fb = new FlxUISpriteButton(0, 0, sprite);
            }
        }
        fb.resize_ratio = resize_ratio;
        fb.resize_point = resize_point;
        fb.autoResizeLabel = resize_label;

        if (setCallback)
        {
            fb.params = params;
        }

        /***Begin graphics loading block***/

        if (data.hasNode.graphic)
        {
            var blank:Bool = U.xml_bool(data.node.graphic.x, "blank");

            if (blank)
            {
                //load blank
                #if neko
                fb.loadGraphicSlice9(["", "", ""], W, H, null, FlxUI9SliceSprite.TILE_NONE, resize_ratio, false, 0, 0, null);
                #else
                fb.loadGraphicSlice9(["", "", ""], W, H, null, FlxUI9SliceSprite.TILE_NONE, resize_ratio);
                #end

            }else{

                var graphic_names:Array<FlxGraphicAsset>=null;
                var slice9_names:Array<Array<Int>>=null;
                var frames:Array<Int>=null;

                if (isToggle) {
                    graphic_names = ["", "", "", "", "", ""];
                    slice9_names= [null, null, null, null, null, null];
                }else {
                    graphic_names = ["", "", ""];
                    slice9_names = [null, null, null];
                }

                //dimensions of source 9slice image (optional)
                var src_w:Int = U.xml_i(data.node.graphic.x, "src_w", 0);
                var src_h:Int = U.xml_i(data.node.graphic.x, "src_h", 0);
                var tile:Int = _loadTileRule(data.node.graphic);

                //custom frame indeces array (optional)
                var frame_str:String = U.xml_str(data.node.graphic.x, "frames",true);
                if (frame_str != "") {
                    frames = new Array<Int>();
                    var arr = frame_str.split(",");
                    for (numstr in arr) {
                        frames.push(Std.parseInt(numstr));
                    }
                }

                if (data.hasNode.scale_src)
                {
                    var scale_:Float = _loadScale(data.node.scale_src, -1);
                    var scale_x:Float = scale_ != -1 ? scale_ : _loadScaleX(data.node.scale_src,-1);
                    var scale_y:Float = scale_ != -1 ? scale_ : _loadScaleY(data.node.scale_src, -1);
                }

                for (graphicNode in data.nodes.graphic) {
                    var graphic_name:String = U.xml_name(graphicNode.x);
                    var image:String = U.xml_str(graphicNode.x, "image");
                    var slice9:Array<Int> = FlxStringUtil.toIntArray(U.xml_str(graphicNode.x, "slice9"));
                    tile = _loadTileRule(graphicNode);

                    var toggleState:Bool = U.xml_bool(graphicNode.x, "toggle");
                    toggleState = toggleState && isToggle;

                    var igfx:String = U.gfx(image);

                    switch(graphic_name) {
                        case "inactive", "", "normal", "up":
                            if (image != "") {
                                if (!toggleState) {
                                    graphic_names[0] = loadScaledSrc(graphicNode,"image","scale_src");
                                }else {
                                    graphic_names[3] = loadScaledSrc(graphicNode,"image","scale_src");
                                }
                            }
                            if(!toggleState)
                            {
                                slice9_names[0] = load9SliceSprite_scaleSub(slice9, graphicNode, graphic_names[0], "image");
                            }
                            else
                            {
                                slice9_names[3] = load9SliceSprite_scaleSub(slice9, graphicNode, graphic_names[3], "image");
                            }
                        case "active", "highlight", "hilight", "over", "hover":
                            if (image != "") {
                                if (!toggleState) {
                                    graphic_names[1] = loadScaledSrc(graphicNode,"image","scale_src");
                                }else {
                                    graphic_names[4] = loadScaledSrc(graphicNode,"image","scale_src");
                                }
                            }
                            if(!toggleState)
                            {
                                slice9_names[1] = load9SliceSprite_scaleSub(slice9, graphicNode, graphic_names[1], "image");
                            }
                            else
                            {
                                slice9_names[4] = load9SliceSprite_scaleSub(slice9, graphicNode, graphic_names[4], "image");
                            }
                        case "down", "pressed", "pushed":
                            if (image != "") {
                                if(!toggleState){
                                    graphic_names[2] = loadScaledSrc(graphicNode,"image","scale_src");
                                }else {
                                    graphic_names[5] = loadScaledSrc(graphicNode,"image","scale_src");
                                }
                            }
                            if(!toggleState)
                            {
                                slice9_names[2] = load9SliceSprite_scaleSub(slice9, graphicNode, graphic_names[2], "image");
                            }
                            else
                            {
                                slice9_names[5] = load9SliceSprite_scaleSub(slice9, graphicNode, graphic_names[5], "image");
                            }
                        case "all":
                            var tilesTall:Int = isToggle ? 6 : 3;

                            var temp:BitmapData = null;

                            //if src_h was provided, manually calculate tilesTall in case it's non-standard (like 4 for instance, like TDRPG uses)
                            if (src_h != 0)
                            {
                                var temp:BitmapData = U.getBmp(igfx);
                                tilesTall = Std.int(temp.height / src_h);
                            }

                            if (image != "") {
                                graphic_names = [loadScaledSrc(graphicNode, "image", "scale_src", 1, tilesTall)];
                            }

                            slice9_names = [load9SliceSprite_scaleSub(slice9, graphicNode, graphic_names[0], "image")];

                            //look at the scaled source to get the absolute correct final values
                            temp = U.getBmp(graphic_names[0]);
                            src_w = temp.width;
                            src_h = Std.int(temp.height / tilesTall);
                    }

                    if (graphic_names[0] != "") {
                        if (graphic_names.length >= 3) {
                            if (graphic_names[1] == "") {		//"over" is undefined, grab "up"
                                graphic_names[1] = graphic_names[0];
                            }
                            if (graphic_names[2] == "") {		//"down" is undefined, grab "over"
                                graphic_names[2] = graphic_names[1];
                            }
                            if (graphic_names.length >= 6) {	//toggle states
                                if (graphic_names[3] == "") {	//"up" undefined, grab "up" (untoggled)
                                    graphic_names[3] = graphic_names[0];
                                }
                                if (graphic_names[4] == "") {	//"over" grabs "over"
                                    graphic_names[4] = graphic_names[1];
                                }
                                if (graphic_names[5] == "") {	//"down" grabs "down"
                                    graphic_names[5] = graphic_names[2];
                                }
                            }
                        }
                    }
                }

                //load 9-slice
                fb.loadGraphicSlice9(graphic_names, W, H, slice9_names, tile, resize_ratio, isToggle, src_w, src_h, frames);
            }
        }else {
            if (load_code == "tab_menu"){
                //load default tab menu graphics
                var graphic_names:Array<FlxGraphicAsset> = [FlxUIAssets.IMG_TAB_BACK, FlxUIAssets.IMG_TAB_BACK, FlxUIAssets.IMG_TAB_BACK, FlxUIAssets.IMG_TAB, FlxUIAssets.IMG_TAB, FlxUIAssets.IMG_TAB];
                var slice9_tab:Array<Int> = FlxStringUtil.toIntArray(FlxUIAssets.SLICE9_TAB);
                var slice9_names:Array<Array<Int>> = [slice9_tab, slice9_tab, slice9_tab, slice9_tab, slice9_tab, slice9_tab];

                //These is/cast checks are here to avoid weeeeird bugs on neko target, which suggests they might also crop up elsewhere
                if (Std.is(fb, FlxUIButton)) {
                    var fbui:FlxUIButton = cast fb;
                    fbui.loadGraphicSlice9(graphic_names, W, H, slice9_names, FlxUI9SliceSprite.TILE_NONE, resize_ratio, isToggle);
                }else if (Std.is(fb, FlxUISpriteButton)) {
                    var fbuis:FlxUISpriteButton = cast fb;
                    fbuis.loadGraphicSlice9(graphic_names, W, H, slice9_names, FlxUI9SliceSprite.TILE_NONE, resize_ratio, isToggle);
                }else {
                    fb.loadGraphicSlice9(graphic_names, W, H, slice9_names, FlxUI9SliceSprite.TILE_NONE, resize_ratio, isToggle);
                }
            }else{
                //load default graphics
                if (W <= 0) W = 80;
                if (H <= 0) H = 20;
                fb.loadGraphicSlice9(null, W, H, null, FlxUI9SliceSprite.TILE_NONE, resize_ratio, isToggle);
            }
        }

        /***End graphics loading block***/

        if (sprite == null)
        {
            if (data != null && data.hasNode.text) {
                formatButtonText(data, fb);
            }else {
                if (load_code == "tab_menu") {
                    fb.up_color = 0xffffff;
                    fb.down_color = 0xffffff;
                    fb.over_color = 0xffffff;
                    fb.up_toggle_color = 0xffffff;
                    fb.down_toggle_color = 0xffffff;
                    fb.over_toggle_color = 0xffffff;
                }
                else
                {
                    // Center sprite icon
                    fb.autoCenterLabel();
                }
            }
        }else {
            fb.autoCenterLabel();
        }

        if (sprite != null && label != "") {
            if(data != null && data.hasNode.text){
                formatButtonText(data, fb);
            }
        }

        var text_x:Int = 0;
        var text_y:Int = 0;
        if (data.x.get("text_x") != null) {
            text_x =  Std.int(_loadWidth(data, 0, "text_x"));
        }else if (data.x.get("label_x") != null) {
            text_x =  Std.int(_loadWidth(data, 0, "label_x"));
        }
        if (data.x.get("text_y") != null) {
            text_y = Std.int(_loadHeight(data, 0, "text_y"));
        }else if (data.x.get("label_y") != null) {
            text_y = Std.int(_loadHeight(data, 0, "label_y"));
        }

        if (Std.is(fb, FlxUISpriteButton))
        {
            var fbs:FlxUISpriteButton = cast fb;
            if(Std.is(fbs.label,FlxSpriteGroup)){
                var g:FlxSpriteGroup = cast fbs.label;
                for (sprite in g.group.members)
                {
                    if (Std.is(sprite, FlxUIText))
                    {
                        //label offset has already been 'centered,' this adjust from there:
                        sprite.offset.x -= text_x;
                        sprite.offset.y -= text_y;
                        break;
                    }
                }
            }else {
                fbs.label.offset.x -= text_x;
                fbs.label.offset.y -= text_y;
                if (toggleSprite != null) {
                    toggleSprite.offset.x -= text_x;
                    toggleSprite.offset.y -= text_y;
                }
            }
        }
        else
        {
            var fbu:FlxUIButton = cast fb;
            //label offset has already been 'centered,' this adjust from there:
            fbu.label.offset.x -= text_x;
            fbu.label.offset.y -= text_y;
        }

        if (sprite != null && toggleSprite != null) {
            fb.toggle_label = toggleSprite;
        }

        if (Std.is(fb, FlxUITypedButton))
        {
            var fuitb:FlxUITypedButton<FlxSprite> = cast fb;
            if (fuitb._assetsToCleanup != null)
            {
                for (key in fuitb._assetsToCleanup)
                {
                    addToCleanup(key);
                }
            }
        }

        return fb;
    }

    override private function _loadNumericStepper(data:Fast, setCallback:Bool = true):IFlxUIWidget
    {

        /*
		 * <numeric_stepper step="1" value="0" min="1" max="2" decimals="1">
		 * 		<text/>
		 * 		<plus/>
		 * 		<minus/>
		 * 		<params/>
		 * </numeric_stepper>
		 *
		 */

        var stepSize:Float = U.xml_f(data.x, "step", 1);
        var defaultValue:Float = U.xml_f(data.x, "value", 0);
        var min:Float = U.xml_f(data.x, "min", 0);
        var max:Float = U.xml_f(data.x, "max", 10);
        var decimals:Int = U.xml_i(data.x, "decimals", 0);
        var percent:Bool = U.xml_bool(data.x, "percent");
        var stack:String = U.xml_str(data.x, "stack",true,"");
        if (stack == "") {
            stack = U.xml_str(data.x, "stacking",true,"");
        }
        stack = stack.toLowerCase();
        var stacking:Int;

        switch(stack) {
            case "horizontal", "h", "horz":
                stacking = FlxUINumericStepper.STACK_HORIZONTAL;
            case "vertical", "v", "vert":
                stacking = FlxUINumericStepper.STACK_VERTICAL;
            default:
                stacking = FlxUINumericStepper.STACK_HORIZONTAL;
        }

        var theText:FlxText = null;
        var buttPlus:FlxUITypedButton<FlxSprite> = null;
        var buttMinus:FlxUITypedButton<FlxSprite> = null;
        var bkg:FlxUISprite = null;

        if (data.hasNode.text) {
            theText = cast _loadThing("text", data.node.text);
        }
        if (data.hasNode.plus) {
            buttPlus = cast _loadThing("button", data.node.plus);
        }
        if (data.hasNode.minus) {
            buttMinus = cast _loadThing("button", data.node.minus);
        }

        var ns:FlxUINumericStepper = new FlxUINumericStepper(0, 0, stepSize, defaultValue, min, max, decimals, stacking, theText, buttPlus, buttMinus, percent);

        if (setCallback) {
            var params:Array<Dynamic> = getParams(data);
            ns.params = params;
        }

        return ns;
    }

    override private function _loadCheckBox(data:Fast):FlxUICheckBox
    {
        var src:String = "";
        var fc:FlxUICheckBox = null;

        var label:String = U.xml_str(data.x, "label");
        var context:String = U.xml_str(data.x, "context", true, "ui");
        var code:String = U.xml_str(data.x, "code", true, "");

        var checked:Bool = U.xml_bool(data.x, "checked", false);

        label = getText(label,context,true,code);

        var labelW:Int = cast _loadWidth(data, 100, "label_width");

        var check_src:String = U.xml_str(data.x, "check_src", true);
        var box_src:String = U.xml_str(data.x, "box_src", true);

        var slice9:String = U.xml_str(data.x, "slice9", true);

        var params:Array<Dynamic> = getParams(data);

        var box_asset:Dynamic = null;
        var check_asset:Dynamic = null;

        /*
		 * For resolution independence you might want scaleable or 9-slice scaleable sprites for box & checkmark.
		 * So in this case, if you supply <box> and <check> nodes instead of "box_src" and "check_src", it will
		 * let you load the box and checkmark using the full power of <sprite> and <9slicesprite> nodes.
		 */

        if (box_src != "")
        {
            //Load standard asset src
            box_asset = U.gfx(box_src);
        }
        else if (data.hasNode.box)
        {
            //We have a custom box node
            if (U.xml_str(data.node.box.x, "slice9") != "")
            {
                //It's a 9-slice sprite, load the custom node
                box_asset = _load9SliceSprite(data.node.box);
            }
            else
            {
                //It's a regular sprite, load the custom node
                box_asset = _loadSprite(data.node.box);
            }
        }

        if (check_src != "")
        {
            //Load standard check src
            check_asset = U.gfx(check_src);
        }
        else if (data.hasNode.check)
        {
            //We have a custom check node
            if (U.xml_str(data.node.check.x, "slice9") != "")
            {
                //It's a 9-slice sprite, load the custom node
                check_asset = _load9SliceSprite(data.node.check);
            }
            else
            {
                //It's a regular sprite, load the custom node
                check_asset = _loadSprite(data.node.check);
            }
        }

        fc = new FlxUICheckBox(0, 0, box_asset, check_asset, label, labelW, params);
        formatButtonText(data, fc);

        var text_x:Int = Std.int(_loadWidth(data, 0, "text_x"));
        var text_y:Int = Std.int(_loadHeight(data, 0, "text_y"));

        fc.textX = text_x;
        fc.textY = text_y;

        fc.text = label;

        fc.checked = checked;

        return fc;
    }

    override private function _loadRadioGroup(data:Fast):FlxUIRadioGroup
    {
        var frg:FlxUIRadioGroup = null;

        var dot_src:String = U.xml_str(data.x, "dot_src", true);
        var radio_src:String = U.xml_str(data.x, "radio_src", true);

        var labels:Array<String> = new Array<String>();
        var names:Array<String> = new Array<String>();

        var W:Int = cast _loadWidth(data, 11, "radio_width");
        var H:Int = cast _loadHeight(data, 11, "radio_height");

        var scrollH:Int = cast _loadHeight(data, 0, "height");
        var scrollW:Int = cast _loadHeight(data, 0, "width");

        var labelW:Int = cast _loadWidth(data, 100, "label_width");

        for (radioNode in data.nodes.radio)
        {
            var name:String = U.xml_name(radioNode.x);
            var label:String = U.xml_str(radioNode.x, "label");

            var context:String = U.xml_str(radioNode.x, "context", true, "ui");
            var code:String = U.xml_str(radioNode.x, "code", true, "");
            label = getText(label,context,true,code);

            names.push(name);
            labels.push(label);
        }

        names.reverse();		//reverse so they match the order entered in the xml
        labels.reverse();

        var y_space:Float = _loadHeight(data, 25, "y_space");

        var params:Array<Dynamic> = getParams(data);


        /*
		 * For resolution independence you might want scaleable or 9-slice scaleable sprites for radio box & dot.
		 * So in this case, if you supply <box> and <dot> nodes instead of "radio_src" and "dot_src", it will
		 * let you load the radio box and dot using the full power of <sprite> and <9slicesprite> nodes.
		 */

        var radio_asset:Dynamic = null;
        if (radio_src != "")
        {
            radio_asset = U.gfx(radio_src);
        }
        else if (data.hasNode.box)
        {
            //We have a custom box node
            if (U.xml_str(data.node.box.x, "slice9") != "")
            {
                //It's a 9-slice sprite, load the custom node
                radio_asset = _load9SliceSprite(data.node.box);
            }
            else
            {
                //It's a regular sprite, load the custom node
                radio_asset = _loadSprite(data.node.box);
            }
        }

        var dot_asset:Dynamic = null;
        if (dot_src != "")
        {
            dot_asset = U.gfx(dot_src);
        }
        else if (data.hasNode.dot)
        {
            //We have a custom check node
            if (U.xml_str(data.node.dot.x, "slice9") != "")
            {
                //It's a 9-slice sprite, load the custom node
                dot_asset = _load9SliceSprite(data.node.dot);
            }
            else
            {
                //It's a regular sprite, load the custom node
                dot_asset = _loadSprite(data.node.dot);
            }
        }

        //if radio_src or dot_src are == "", then leave radio_asset/dot_asset == null,
        //and FlxUIRadioGroup will default to defaults defined in FlxUIAssets

        var prevOffset:FlxPoint = null;
        var nextOffset:FlxPoint = null;

        if (data.hasNode.button)
        {
            for (btnNode in data.nodes.button)
            {
                var name:String = U.xml_name(btnNode.x);
                if (name == "previous" || name == "prev")
                {
                    prevOffset = FlxPoint.get(U.xml_f(btnNode.x, "x"),U.xml_f(btnNode.x,"y"));
                }
                else if (name == "next")
                {
                    nextOffset = FlxPoint.get(U.xml_f(btnNode.x, "x"),U.xml_f(btnNode.x,"y"));
                }
            }
        }

        frg = new FlxUIRadioGroup(0, 0, names, labels, null, y_space, W, H, labelW, prevOffset, nextOffset);
        frg.params = params;

        if (radio_asset != "" && radio_asset != null)
        {
            frg.loadGraphics(radio_asset,dot_asset);
        }

        var text_x:Int = Std.int(_loadWidth(data, 0, "text_x"));
        var text_y:Int = Std.int(_loadHeight(data, 0, "text_y"));

        var radios = frg.getRadios();
        var i:Int = 0;
        var styleSet:Bool = false;

        var radioList = data.x.elementsNamed("radio");
        var radioNode:Xml = null;

        for (k in 0...radios.length)
        {
            var fo = radios[(radios.length - 1) - k];
            radioNode = radioList.hasNext() ? radioList.next() : null;
            if (fo != null)
            {
                if (Std.is(fo, FlxUICheckBox))
                {
                    var fc:FlxUICheckBox = cast(fo, FlxUICheckBox);
                    var t:FlxText = formatButtonText(data, fc);
                    if (t != null && styleSet == false)
                    {
                        var fd = FontDef.copyFromFlxText(t);
                        var bd = new BorderDef(t.borderStyle, t.borderColor, t.borderSize, t.borderQuality);
                        frg.activeStyle = new CheckStyle(0xffffff, fd, t.alignment, t.color, bd);
                        styleSet = true;
                    }
                    fc.textX = text_x;
                    fc.textY = text_y;
                    i++;
                    if (radioNode != null)
                    {
                        _loadTooltip(fc, new Fast(radioNode));
                    }
                }
            }
        }

        if (scrollW != 0)
        {
            frg.fixedSize = true;
            frg.width = scrollW;
        }
        if (scrollH != 0)
        {
            frg.fixedSize = true;
            frg.height = scrollH;
        }

        return frg;
    }

    override private function _loadInputText(data:Fast):IFlxUIWidget
    {
        var text:String = U.xml_str(data.x, "text");
        var context:String = U.xml_str(data.x, "context", true, "ui");
        var code:String = U.xml_str(data.x, "code", true, "");
        text = getText(text,context, true, code);

        var W:Int = Std.int(_loadWidth(data, 100));
        var H:Int = Std.int(_loadHeight(data, -1));

        var the_font:String = _loadFontFace(data);

        var align:String = U.xml_str(data.x, "align"); if (align == "") { align = null;}
        var size:Int = Std.int(_loadHeight(data, 8, "size"));
        var color:Int = _loadColor(data);

        var border:BorderDef = _loadBorder(data);

        var backgroundColor:Int = U.parseHex(U.xml_str(data.x, "background", true, "0x00000000"), true, true, 0x00000000);
        var passwordMode:Bool = U.xml_bool(data.x, "password_mode");

        var ft:IFlxUIWidget;
        var fti:FlxUIInputText = new FlxUIInputText(0, 0, W, text, size, color, backgroundColor);
        fti.passwordMode = passwordMode;

        var force_case:String = U.xml_str(data.x, "force_case", true, "");
        var forceCase:Int;
        switch(force_case)
        {
            case "upper", "upper_case", "uppercase": forceCase = FlxInputText.UPPER_CASE;
            case "lower", "lower_case", "lowercase": forceCase = FlxInputText.LOWER_CASE;
            case "u", "l":
                throw new Error("FlxUI._loadInputText(): 1 letter values have been deprecated (force_case attribute).");
            default: forceCase = FlxInputText.ALL_CASES;
        }

        var filter:String = U.xml_str(data.x, "filter", true, "");
        var filterMode:Int;
        while (filter.indexOf("_") != -1)
        {
            filter = StringTools.replace(filter, "_", "");	//strip out any underscores
        }

        switch(filter)
        {
            case "alpha", "onlyalpha": filterMode = FlxInputText.ONLY_ALPHA;
            case "num", "numeric", "onlynumeric": filterMode = FlxInputText.ONLY_NUMERIC;
            case "alphanum", "alphanumeric", "onlyalphanumeric": filterMode = FlxInputText.ONLY_ALPHANUMERIC;
            case "a", "n", "an":
                throw new Error("FlxUI._loadInputText(): 1 letter values have been deprecated (filter attribute).");
            default: filterMode = FlxInputText.NO_FILTER;
        }

        fti.setFormat(the_font, size, color, align);
        fti.forceCase = forceCase;
        fti.filterMode = filterMode;
        border.apply(fti);
        fti.drawFrame();
        ft = fti;

        if (data.hasNode.param)
        {
            var params = getParams(data);
            var ihp:IHasParams = cast ft;
            ihp.params = params;
        }

        if (H > 0 && ft.height != H)
        {
            if (Std.is(ft, IResizable))
            {
                var r:IResizable = cast ft;
                r.resize(r.width, H);
            }
        }

        return ft;
    }

    override private function _loadText(data:Fast):IFlxUIWidget
    {
        var text:String = U.xml_str(data.x, "text");
        var context:String = U.xml_str(data.x, "context", true, "ui");
        var code:String = U.xml_str(data.x, "code", true, "");
        text = getText(text,context, true, code);

        var W:Int = Std.int(_loadWidth(data, 100));
        var H:Int = Std.int(_loadHeight(data, -1));

        var the_font:String = _loadFontFace(data);

        var input:Bool = U.xml_bool(data.x, "input");
        if(input)
        {
            throw new Error("FlxUI._loadText(): <text> with input has been deprecated. Use <input_text> instead.");
        }

        var align:String = U.xml_str(data.x, "align"); if (align == "") { align = null;}
        var size:Int = Std.int(_loadHeight(data, 8, "size", "floor"));

        var color:Int = _loadColor(data);

        var border:BorderDef = _loadBorder(data);

        var backgroundColor:Int = U.parseHex(U.xml_str(data.x, "background", true, "0x00000000"), true, true, 0x00000000);

        var ft:IFlxUIWidget;
        var ftu:FlxUIText = new FlxUIText(0, 0, W, text, size);
        ftu.setFormat(the_font, size, color, align);
        border.apply(ftu);
        ftu.drawFrame();
        ft = ftu;

        if (data.hasNode.param) {
            var params = getParams(data);
            var ihp:IHasParams = cast ft;
            ihp.params = params;
        }

        if (H > 0 && ft.height != H)
        {
            if (Std.is(ft, IResizable))
            {
                var r:IResizable = cast ft;
                r.resize(r.width, H);
            }
        }

        //force text redraw
        ftu.text = " ";
        ftu.text = text;

        return ft;
    }

    override private function _changeThing(data:Fast):Void
    {
        var name:String = U.xml_name(data.x);
        var thing = getAsset(name);
        if (thing == null)
        {
            return;
        }

        var new_width:Float = -1;
        var new_height:Float = -1;

        var context:String = "";
        var code:String = "";

        var labelStyleChanged:Bool = false;

        for (attribute in data.x.attributes())
        {
            switch(attribute)
            {
                case "text": if (Std.is(thing, FlxUIText))
                {
                    var text = U.xml_str(data.x, "text");
                    context = U.xml_str(data.x, "context", true, "ui");
                    var t:FlxUIText = cast thing;
                    code = U.xml_str(data.x, "code", true, "");
                    t.text = getText(text, context, true, code);
                }
                case "label": var label = U.xml_str(data.x, "label");
                    context = U.xml_str(data.x, "context", true, "ui");
                    code = U.xml_str(data.x, "code", true, "");
                    label = getText(label, context, true, code);
                    if (Std.is(thing, ILabeled))
                    {
                        var b:ILabeled = cast thing;
                        b.getLabel().text = label;
                    }
                case "width": new_width = _loadWidth(data);
                case "height": new_height = _loadHeight(data);
            }
        }
        if (Std.is(thing, IResizable))
        {
            var ir:IResizable = cast thing;
            if (new_width != -1 || new_height != -1)
            {
                if (new_width == -1) { new_width = ir.width; }
                if (new_height == -1) { new_height = ir.height; }
                ir.resize(new_width, new_height);
            }
        }

        if (data.hasNode.param)
        {
            if (Std.is(thing, IHasParams))
            {
                var ihp:IHasParams = cast thing;
                ihp.params = getParams(data);
            }
        }
    }

    override private function _changeParamsThing(data:Fast):Void
    {
        var name:String = U.xml_name(data.x);
        var thing:IFlxUIWidget = getAsset(name);
        if (thing == null) {
            return;
        }

        if (!Std.is(thing, IHasParams)) {
            return;
        }

        var i:Int = 0;
        if (Std.is(thing, IHasParams)) {

        }
        var ihp:IHasParams = cast thing;
        ihp.params = getParams(data);
    }

}
