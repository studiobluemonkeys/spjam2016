package bluemonkeys;

import haxe.macro.Expr;
import haxe.macro.Context;
#if macro
class Macros {
    /**
    * This macro should be used with MenuScreen class. It adds screen definitions to be used on ui scripts.
    * It finds constructor function and injects screen definition at end. It runs on compile time only.
    *
    * @param screenName    name used to instantiate screen on script
    * @param screenClass   name of screen class
    **/
    public static macro function RegisterScreen(screenName:String, screenClass:String) :Array<Field>{
        var fields = Context.getBuildFields();
        for (field in fields) {
            // find constructor
            if (field.name == "new"){
                // get function expressions
                switch(field.kind){
                case FFun(f):
                    // append this definition at the end of function
                    f.expr = macro {
                        ${f.expr};
                        interp.variables[$v{screenName}] = $i{screenClass};
                    }
                default:
                }
                // we don't need to search for any other field, so stop searching here
                break;
            }
        }
        // return changed fields
        return fields;
    }
}
#end