package bluemonkeys;

import flixel.math.FlxPoint;
import flixel.FlxSprite;
import flixel.system.FlxAssets.FlxGraphicAsset;


//TODO: Maybe this is better if used as a component
class AttractableObject extends FlxSprite {
    private var currentForces:Map<Player, FlxPoint> = new Map<Player, FlxPoint>();
    public function setForce (player:Player, force:FlxPoint){
        currentForces[player] = force;
    }
    public function new(X:Float, Y:Float, ?SimpleGraphic:FlxGraphicAsset){
        super(X,Y, SimpleGraphic);
        // raise values if you want to stop objects earlier
//        drag.set(10, 10);
    }
    override public function update(elapsed:Float) {
        if (Reg.alternativeForceModel) {
            acceleration.set();
            for (force in currentForces) {
                acceleration.addPoint(force);
            }
        } else {
            velocity.set();
            for (force in currentForces) {
                velocity.addPoint(force);
            }
        }
        super.update(elapsed);
    }
}