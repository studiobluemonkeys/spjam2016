package bluemonkeys.lib;

import flixel.FlxG;
import flixel.system.FlxSound;

class SingleSfxPlayer {
    private var sound:FlxSound = null;
    private var loop           = false;
    private var autoDestroy    = false;
    private var forceRestart   = true;
    private var volume         = 0.8;
    private var asset          = "";

    public function new(sound:String) {
        asset = sound;
    }

    public function play() {
        if(sound == null) { 
            sound = FlxG.sound.play(asset, volume, loop, autoDestroy);            
        }
        else {
            sound.play(forceRestart, 0);
        }
    }

    public function destroy() {
        sound.destroy();
    }
}
