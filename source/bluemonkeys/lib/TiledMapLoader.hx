package bluemonkeys.lib;

import flixel.addons.tile.FlxTileSpecial;
import flixel.addons.tile.FlxTilemapExt;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.math.FlxRect;
import flixel.util.FlxColor;
import flixel.util.FlxDestroyUtil;
import flixel.util.typeLimit.OneOfTwo;
import haxe.crypto.Base64;
import haxe.io.Path;
import haxe.zip.InflateImpl;
import openfl.Assets;

using StringTools;

/**
 * This is the type of callback used to construct objects in object layer
 * It receives 3 maps:
 * <ul>
 * <li>first one is the object default properties</li>
 * <li>second one is the object custom properties</li>
 * <li>third one is the layer custom properties</li>
 * </ul>
 * The callback must return a class that implements `IFlxDestroyable` (i.e. it must have a `destroy` function).
 */
typedef ObjectFactory = Map<String, String>->Map<String, String>->Map<String, String>->IFlxDestroyable;

/**
 * This class should make easier to load and manage Tiled maps loading
 * It's very non-intrusive yet simple to use.
 * Example:
 * ```
 * var mapLoader = new TiledMapLoader("assets/data/lvl1_full.tmx", createObjectCallback);
 * ```
 * Once instantiated, it loads all (object and tile) layers, making them accessible using
 * properties `tileLayers` and `objectLayers`. Objects are loaded using a callback function
 * of type `ObjectFactory`.
 *
 * Some things were not implemented yet, like:
 *   You can use only one spritesheet per layer right now
 *   It only loads correctly orthogonal maps
 *   It does not consider render order right now
 *   It does not handle spacing
 */
class TiledMapLoader implements IFlxDestroyable{

    private static inline var FLIP_X    = 0x80;
    private static inline var FLIP_Y    = 0x40;
    private static inline var FLIP_DIAG = 0x20;
    private static inline var diagAndX = TiledMapLoader.FLIP_DIAG | TiledMapLoader.FLIP_X;
    private static inline var diagAndY = TiledMapLoader.FLIP_DIAG | TiledMapLoader.FLIP_Y;

    var useNape:Bool = false;

    /**
     * Holds parsed tile layers (each layer is an instance of `TiledLayer`).
     */
    public var tileLayers(default, null):Array<TiledLayer> = new Array<TiledLayer>();

    /**
     * Holds parsed object layers (each layer is a list of `IFlxDestroyable` objects).
     */
    public var objectLayers(default, null):Array<Array<IFlxDestroyable>> = new Array<Array<IFlxDestroyable>>();

    /**
     * Holds parsed image layers
     */
    public var imageLayers(default, null):Array<TiledImageLayer> = new Array<TiledImageLayer>();

    /**
     * Holds map custom properties
     */
    public var customProperties(default, null):Map<String, String> = new Map<String, String>();

    /**
     * Holds tilesets (for whatever reason the user might need to get them)
     */
    public var tilesets(default, null):Map<String, Tileset> = new Map<String, Tileset>();

    /**
     * Map orientation, can be orthogonal, isometric or hexagonal.
     * (Currently, works correctly only using orthogonal orientation)
     */
    public var orientation(default, null):String;

    /**
     * Map width in tiles
     */
    public var mapWidth(default, null):Int;

    /**
     * Map height in tiles
     */
    public var mapHeight(default, null):Int;

    /**
     * Tile width in pixels
     */
    public var tileWidth(default, null):Int;

    /**
     * Tile height in pixels
     */
    public var tileHeight(default, null):Int;

    /**
     * Render order, can be right-down, right-up, left-down or left-up
     * (right now it's ignored by tilemap constructor)
     */
    public var renderOrder(default, null):String;

    /**
     * Background color
     */
    public var bgColor(default, null):FlxColor;

    /**
     * Constructor
     *
     * @param tiledFile             which tiled map to load
     * @param suggestedBasePath     base path used to load tiled images (automatically get if loading a file)
     * @param useNape               if we need to create Nape polygons for each tile and object
     * @param objectLoaderCallback  callback used to load object layers. Returned value will be used on object layer array
     */
    public function new(tiledFile:OneOfTwo<String, Xml>, ?suggestedBasePath:String, ?useNape:Bool, ?objectLoaderCallback:ObjectFactory) {
        var source:Xml = null;
        var basePath = "";

        if (Std.is(tiledFile, String))
        {
            source = Xml.parse(Assets.getText(tiledFile));

            // get base path to find spritesheet images
            basePath = Assets.getPath(tiledFile);
            var lastSlash:Int = basePath.lastIndexOf("/");
            basePath = basePath.substr(0, lastSlash+1);
        }
        else if (Std.is(tiledFile, Xml))
        {
            source = tiledFile;
            basePath = suggestedBasePath;
        }

        source = source.firstElement();

        getMapInfo(source);
        customProperties = getCustomProperties(source);
        loadTilesets(source, basePath);
        loadLayers(source, objectLoaderCallback);
    }

    /**
     * Will destroy all layers, even object and image layers.
     */
    public function destroy() {
        tileLayers = FlxDestroyUtil.destroyArray(tileLayers);
        while (objectLayers.length > 0){
            FlxDestroyUtil.destroyArray(objectLayers.pop());
        }
        objectLayers = null;
        imageLayers = FlxDestroyUtil.destroyArray(imageLayers);
    }

    /**
     * Updates world bounds according to map size(important to keep physics working)
     */
    public inline function updateWorldBounds() {
        FlxG.worldBounds.set(0, 0, mapWidth * tileWidth, mapHeight * tileHeight);
    }

    private function getMapInfo(source:Xml) {
        orientation = source.get("orientation");
        mapWidth = Std.parseInt(source.get("width"));
        mapHeight = Std.parseInt(source.get("height"));
        tileWidth = Std.parseInt(source.get("tilewidth"));
        tileHeight = Std.parseInt(source.get("tileheight"));
        renderOrder = source.get("renderorder");
        bgColor = FlxColor.fromString(source.exists("backgroundcolor") ? source.get("backgroundcolor") : "#000000");
    }

    private function getCustomProperties(source:Xml):Map<String, String> {
        var propertyList:Map<String, String> = new Map<String, String>();
        var properties = source.elementsNamed("properties");
        // no properties, ignore it
        if (!properties.hasNext()) return propertyList;

        for (prop in properties.next().elements()) {
            propertyList[prop.get("name")] = prop.get("value");
        }
        return propertyList;
    }

    private function getDefaultProperties(source:Xml) {
        return [for (attrName in source.attributes()) attrName=>source.get(attrName)];
    }

    private function loadTilesets(source:Xml, basePath:String) {
        for (tileset in source.elementsNamed("tileset")) {
            var imageNode = tileset.elementsNamed("image").next();
            var tileset:Tileset = new Tileset(
                Std.parseInt(tileset.get("firstgid")),
                tileset.get("name"),
                Std.parseInt(tileset.get("tilewidth")),
                Std.parseInt(tileset.get("tileheight")),
                Std.parseInt(tileset.get("tilecount")),
                Std.parseInt(tileset.get("columns")),
                basePath + imageNode.get("source"),
                Std.parseInt(imageNode.get("width")),
                Std.parseInt(imageNode.get("height")),
                Std.parseInt(if (tileset.exists("spacing")) tileset.get("spacing") else "0")
            );
            tilesets[tileset.name] = tileset;
        }
    }
    // TODO: fix spritesheets using spacing
    private function loadLayers(source:Xml, ?objectLoaderCallback:ObjectFactory) {
        for (layer in source.elements()) {
            // get layer properties
            var layerCustomProperties = getCustomProperties(layer);
            var activateCollisions:Bool = true;
            // toggles colliders on this layer only if the property exists
            if (layerCustomProperties.exists("Collide")) {
                activateCollisions = layerCustomProperties["Collide"] == "true";
            }

            switch (layer.nodeName) {
            case "layer": // tile layer
                FlxG.log.warn('Loading layers...');
                var parsedLayer:TiledLayer = new TiledLayer(getDefaultProperties(layer), layerCustomProperties);
                // get layer data
                var dataNode:Xml = layer.elementsNamed("data").next();
                switch(dataNode.get("encoding")) {
                case "base64":
                    var data:Array<Int> = [];
                    // get map data
                    var convertedData = Base64.decode(dataNode.firstChild().nodeValue.ltrim().rtrim());
                    // check if we need to uncompress it
                    switch(dataNode.get("compression")) {
                    case "zlib":
                        convertedData = InflateImpl.run(new haxe.io.BytesInput(convertedData));
                    case "gzip":
                        #if format
                        convertedData = (new format.gz.Reader(new haxe.io.BytesInput(convertedData))).read().data;
                        #else
                        throw "You need 'format' library included to load gzipped maps.";
                        #end
                    default: // no compression
                    }
                    // get map data from bytes
                    // each tile is stored in an unsigned 32-bit number. The 3 latest bits store flip data
                    // and they need to be cleaned to get correct tile IDs
                    var i:Int = 0;
                    var specialTiles:Array<FlxTileSpecial> = [];
                    while (convertedData.length > i) {
                        var specialTile:FlxTileSpecial = null;
                        var tile = convertedData.getInt32(i);
                        // remove any possible flip data and add to tile data array
                        data.push(tile & 0x00FFFFFF);
                        // check flips
                        var flipX = false, flipY = false;
                        var rotation = FlxTileSpecial.ROTATE_0;
                        var flipData = tile >>> 24;
                        if (flipData != 0) {
                            /*
                             Flip data is contained on latest 3 bits, and they mean (MSB to LSB):
                             1 - flip on x axis
                             2 - flip on y axis
                             3 - flip (anti)diagonally [\]

                             diagonal operation must be first.

                             Since Flixel does not diagonally flip sprites, we need to handle diagonal flips separately.

                             Our strategy is:
                             If diag flip is off, just consider axis flip bits.
                             Otherwise:
                                If just diagonal flip, turn sprite 90º CW and flip on x axis
                                If diag + x flip, turn sprite 90º CW
                                If diag + y flip, turn sprite 270º CW
                                Otherwise (all flips), turn sprite 270º CW and flip on x axis
                            */

                            if (flipData & FLIP_DIAG != 0){
                                // has diag flip

                                switch(flipData){
                                case TiledMapLoader.FLIP_DIAG:
                                        // diag = 90º + x
                                        flipX = true;
                                        rotation = FlxTileSpecial.ROTATE_90;
                                case (diagAndX):
                                    // diag + x = 90º
                                    rotation = FlxTileSpecial.ROTATE_90;
                                case (diagAndY):
                                    // diag + y = 270º
                                    rotation = FlxTileSpecial.ROTATE_270;
                                default:
                                    // diag + x + y = 270º + x
                                    flipX = true;
                                    rotation = FlxTileSpecial.ROTATE_270;
                                }
                            } else {
                                // no diag flip
                                flipX = flipData & FLIP_X != 0;
                                flipY = flipData & FLIP_Y != 0;
                            }
                            specialTile = new FlxTileSpecial(tile & 0x00FFFFFF, flipX, flipY, rotation);
                        }
                        specialTiles.push(specialTile);
                        i+= 4;
                    }
                    // we need to know which tileset will be used to render this tilemap
                    var selectedTileset:Tileset = getTilesetByGID(getFirstValidGID(data));
                    // and finally, add map to tile layers list
                    parsedLayer.loadMapFromArray(data, mapWidth, mapHeight, selectedTileset.image.source, tileWidth, tileHeight, selectedTileset.firstGID, 1, if (activateCollisions) 1 else 999999);
                    // add flip information
                    if (specialTiles.length > 0) {
                        parsedLayer.setSpecialTiles(specialTiles);
                    }
                case "csv":
                    // no need further processing, just remove spaces
                    var data:String = dataNode.firstChild().nodeValue.ltrim().rtrim();

                    var selectedTileset:Tileset = getTilesetByGID(
                        getFirstValidGID(
                            data.split(",")
                            .map(function(el:String):Int{
                                return Std.parseInt(el);
                            })
                        )
                    );
                    parsedLayer.loadMapFromCSV(data, selectedTileset.image.source, tileWidth, tileHeight, selectedTileset.firstGID, 1, if (activateCollisions) 1 else 999999);
                default: // xml encoding
                    var data:Array<Int> = [for (el in dataNode.elementsNamed("tile")) Std.parseInt(el.get("gid"))];
                    var selectedTileset:Tileset = getTilesetByGID(getFirstValidGID(data));
                    parsedLayer.loadMapFromArray(data, mapWidth, mapHeight, selectedTileset.image.source, tileWidth, tileHeight, selectedTileset.firstGID, 1, if (activateCollisions) 1 else 999999);
                }
                tileLayers.push(parsedLayer);
            case "objectgroup": // object layer
                if (objectLoaderCallback != null){
                    var objects:Array<IFlxDestroyable> = [];
                    for(object in layer.elementsNamed("object")) {
                        objects.push(objectLoaderCallback(getDefaultProperties(object), getCustomProperties(object), layerCustomProperties));
                    }
                    objectLayers.push(objects);
                }
            case "imagelayer": // image layer
                for (img in layer.elementsNamed("image")) {
                    var loadedImg:TiledImageLayer = new TiledImageLayer(img.get("source"), getDefaultProperties(layer), layerCustomProperties);
                    imageLayers.push(loadedImg);
                }
            default: // not a layer, ignore
                continue;
            }
        }
    }

    private function getFirstValidGID(data:Array<Int>) {
        for (el in data) {
            if (el != 0) {
                return el;
            }
        }
        return 0;
    }

    private function getTilesetByGID(id:Int):Tileset {
        var selectedTileset:Tileset = null;
        for (tileset in tilesets){
            if (selectedTileset == null) selectedTileset = tileset;
            if (tileset.firstGID <= id && selectedTileset.firstGID <= tileset.firstGID) {
                selectedTileset = tileset;
            }
        }
        return selectedTileset;
    }
}

/**
 * This class holds tileset properties. `TiledMapLoader` will instantiate them automatically.
 */
class Tileset {
    public var firstGID  (default, null):Int;
    public var name      (default, null):String;
    public var tileWidth (default, null):Int;
    public var tileHeight(default, null):Int;
    public var spacing   (default, null):Int;
    public var tileCount (default, null):Int;
    public var columns   (default, null):Int;
    public var image     (default, null):{source:String, width:Int, height:Int};

    @:allow(bluemonkeys.lib.TiledMapLoader)
    private function new(firstGID:Int, name:String, tileWidth:Int, tileHeight:Int, tileCount:Int, columns:Int, imageSrc:String, imageWidth:Int, imageHeight:Int, spacing:Int) {
        this.firstGID = firstGID;
        this.name = name;
        this.tileWidth = tileWidth;
        this.tileHeight = tileHeight;
        this.spacing = spacing;
        this.tileCount = tileCount;
        this.columns = columns;
        this.image = {source : Path.normalize(imageSrc), width : imageWidth, height : imageHeight};
    }
}

/**
 * This class holds populated tiles layers. It's actually a `FlxTilemap` with
 * added field for holding custom properties, so it's fully compatible with
 * `FlxTilemap`. `TiledMapLoader` will instantiate them automatically.
 */
class TiledLayer extends FlxTilemapExt {
    /**
     * A map with custom properties of layer
     */
    public var defaultProperties(default, null):Map<String, String>;
    public var customProperties(default, null):Map<String, String>;

    @:allow(bluemonkeys.lib.TiledMapLoader)
    private function new(defaultProperties:Map<String, String>, customProperties:Map<String, String>){
        super();
        this.defaultProperties = defaultProperties;
        this.customProperties = customProperties;
    }
}

/**
 * Holds image layers. Like `TiledLayer`, just adds properties to
 * `FlxGroup` class.
 **/
class TiledImageLayer extends FlxSprite {
    /**
     * A map with custom properties of layer
     */
    public var defaultProperties(default, null):Map<String, String>;
    public var customProperties(default, null):Map<String, String>;

    @:allow(bluemonkeys.lib.TiledMapLoader)
    private function new(imageSource:String, defaultProperties:Map<String, String>, customProperties:Map<String, String>){
        super(imageSource);
        this.defaultProperties = defaultProperties;
        this.customProperties = customProperties;
    }
}