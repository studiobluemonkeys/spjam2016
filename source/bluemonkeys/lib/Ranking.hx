package bluemonkeys.lib;
/**
 * Ranking handling class
 * Saves ranking on default flixel save data
 */
import flixel.FlxG;
class Ranking {
    private static inline var MAX_ENTRIES = 20;
    // returns value position in ranking
    public static function addValue(val:Int):Int {
        var current = getRanking();
        current.push(val);
        current.sort(sortFunction);
        current = filterRanking(current);
        FlxG.save.data.ranking = current;
        FlxG.save.flush();
        return current.lastIndexOf(val);
    }
    // sort rank (bigger first)
    private inline static function sortFunction(a:Int, b:Int):Int return b - a;

    // limit saved rank to MAX_ENTRIES
    private static function filterRanking(list:Array<Int>) {
        return list.slice(0, MAX_ENTRIES);
    }

    // add several values at once, returns indexes of added ranks
    public static function addValues(values:Array<Int>):Array<Int>{
        var current = getRanking();
        current = current.concat(values);
        current.sort(sortFunction);
        current = filterRanking(current);
        var ret = new Array<Int>();
        for (val in values) {
            ret.push(current.lastIndexOf(val));
        }
        FlxG.save.data.ranking = current;
        FlxG.save.flush();
        return ret;
    }

    // get list of saved positions
    public static function getRanking():Array<Int> {
        var current = FlxG.save.data.ranking;
        return if (current == null) [] else current;
    }
}
