package bluemonkeys;

import bluemonkeys.screens.MenuScreen;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.addons.transition.FlxTransitionableState;
import flixel.util.FlxTimer;

class SplashState extends FlxTransitionableState {
    private var splashTimeout = 5;
    private var nextState:Class<FlxState> = MenuScreen;
    private var nextStateParams = ["main_menu"];
    public override function create() {
        var splashImage = new FlxSprite(0, 0, "assets/images/bluemonkeys-logo.jpg");
        splashImage.height = FlxG.height;
        splashImage.screenCenter();
        this.bgColor = 0xFFFFFF;
        add(splashImage);
        var matchTimer = new FlxTimer();
        matchTimer.start(splashTimeout, changeState, 0);
        transitionIn();
    }
    override public function update (elapsed:Float) {
        if (FlxG.keys.firstJustPressed() != -1) {
            changeState();
        }
    }
    private function changeState(?timer:FlxTimer) {
        var state = Type.createInstance(nextState, nextStateParams);
        FlxG.switchState(state);
    }

}