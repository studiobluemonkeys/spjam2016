package bluemonkeys;

import flixel.input.keyboard.FlxKey;
import flixel.FlxG;
import flixel.FlxState;
import flixel.addons.transition.FlxTransitionableState;
import flixel.addons.transition.TransitionData;
import flixel.graphics.FlxGraphic;
import flixel.addons.transition.FlxTransitionSprite;
import flixel.math.FlxPoint;
import flixel.util.FlxColor;

import bluemonkeys.level.BaseLevelState;

/**
* FirstState - Use this state to initialize anything
* that need to be initialized AFTER FlxGame is initialized
* but before any other state.
* */
class FirstState extends FlxState {
    /* holds the state that will be loaded after this ends initialization */
    private static var FIRST_STATE:Class<FlxState> = bluemonkeys.SplashState;
    private static var FIRST_STATE_PARAMS:Array<Dynamic> = [];
    override public function create():Void {
        super.create();
        // any game initialization should be done here.
        #if silent
        FlxG.sound.muted = true;
        #end

        // since 0 conflicts with numbers when configuring match time, let's disable all sound shortcuts
        FlxG.sound.muteKeys = null;
        FlxG.sound.volumeUpKeys = null;
        FlxG.sound.volumeDownKeys = null;

        // load stored preferences
        loadPreferences();

        // set default transition for all transitionable screens
        FlxTransitionableState.defaultTransIn  = new TransitionData(TILES, FlxColor.BLACK, 0.3, new FlxPoint(1, 1));
        FlxTransitionableState.defaultTransOut = new TransitionData(TILES, FlxColor.BLACK, 0.3, new FlxPoint(1, 1));

        var diamond:FlxGraphic = FlxGraphic.fromClass(GraphicTransTileDiamond);
        diamond.persist = true;
        diamond.destroyOnNoUse = false;

        FlxTransitionableState.defaultTransIn.tileData = { asset:diamond, width:32, height:32 };
        FlxTransitionableState.defaultTransOut.tileData = { asset:diamond, width:32, height:32 };

        var state = Type.createInstance(FIRST_STATE, FIRST_STATE_PARAMS);
        // LevelManager.shared().setCurrentState(cast(state, demo.TiledStageDemoState));
        FlxG.switchState(state);
    }
    private function loadPreferences() {
        if (FlxG.save.data.time_limit != null) {
            Reg.TIME_LIMIT = FlxG.save.data.time_limit;
        }
        if (FlxG.save.data.alt_force != null) {
            Reg.alternativeForceModel = FlxG.save.data.alt_force;
        }
        #if !silent
        if (FlxG.save.data.sound_enabled != null) {
            FlxG.sound.muted = !(FlxG.save.data.sound_enabled);
        }
        #end
    }
}
