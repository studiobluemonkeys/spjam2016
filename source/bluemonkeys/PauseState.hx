package bluemonkeys;

import flixel.FlxG;
import flixel.addons.ui.FlxUIPopup;

class PauseState extends FlxUIPopup {
    override public function create() {
        _xml_id = "pause_popup";
        super.create();
    }
    override public function getEvent(id:String, sender:Dynamic, data:Dynamic, ?eventParams:Array<Dynamic>) {
        if (id == "click_button") {
            close();
        }
    }
}
