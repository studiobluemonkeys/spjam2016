package bluemonkeys.events;

import openfl.events.Event;

class PauseEvent extends Event {
    public var paused(default, null):Bool;
    public function new(paused:Bool) {
        super(Reg.PAUSE_EVENT);
        this.paused = paused;
    }
    override public function clone():Event {
        return new PauseEvent(paused);
    }

    override public function toString():String {
        return '[Event type=${type} bubbles=${bubbles} cancelable=${cancelable} paused=${paused}]';
    }
}