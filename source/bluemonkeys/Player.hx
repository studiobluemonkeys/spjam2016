package bluemonkeys;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.math.FlxPoint;
import flixel.math.FlxVector;
import flixel.math.FlxMath;
import flixel.util.FlxTimer;
import flixel.ui.FlxBar;
import bluemonkeys.lib.SingleSfxPlayer;
import flixel.graphics.frames.FlxAtlasFrames;

enum ForceType {
    NONE;
    ATTRACT;
    REPULSE;
}

class Player extends FlxSprite {
    private var force:ForceType = ForceType.NONE;

    private var forceValue:Float = 8000;

    private var attractionFactor:Float = 10;
    private var attractionMultiplier:Float = 1000;

    static var ZERO_VECTOR = new FlxVector();

    private var speed:Float = 300;
    private var speedModifierOn:Bool = false;
    private var forceModifierOn:Bool = false;
    private var maxHealth:Int = 100;
    public var healthBar(default, null):FlxBar;
    private var id:Int;
    private var spawnPoint:FlxPoint;
    private var sfxPlayer:SingleSfxPlayer;

    public function new(x:Float, y:Float, id:Int)
    {
        super(x, y, "assets/images/personagem_01.png");
        this.id = id;
        spawnPoint = new FlxPoint(x, y);
        sfxPlayer = new SingleSfxPlayer("assets/sounds/death.wav");
        // reset score
        Reg.scores[id] = 0;
        health = maxHealth;
        healthBar = new FlxBar(0, 0, 50, this, "health", true);
        healthBar.solid = false;
        var barX:Int = cast -25+this.width/2;
        healthBar.trackParent(barX, -10);
        healthBar.killOnEmpty = false;

        setupBoundingBox();
        setupAnimation();

        playFrontAnim();

        this.x -= this.width/2;
        this.y -= this.height/2;
    }

    private function setupBoundingBox():Void {
        // Tweak the bounding box for better feel
        width = 32;
        height = 32;
        offset.x = 0;
        offset.y = 0;
    }

    private function setupAnimation():Void {
        var atlasFrames:FlxAtlasFrames = FlxAtlasFrames.fromSparrow("assets/images/chars_ss.png", "assets/images/chars_ss.xml");
        this.frames = atlasFrames;
        var framerate:Int = 1;
        var looped:Bool = false;

        var frontFrame = 2 * id + 1;
        var backFrame = 2 * id;

        animation.add("front", [frontFrame], framerate, looped);
        animation.add("back", [backFrame], framerate, looped);
        // animation.add("walk", [6, 7, 8, 9 ,10 , 11, 12, 13, 14, 15], framerate, looped);
    }

    /*** Animation Functions ***/

    private function playFrontAnim():Void {
        animation.play("front");
    }

    private function playBackAnim():Void {
        animation.play("back");
    }
    /*** End of Animation Functions ***/

    public function addScore(value:Int) {
        Reg.scores[id] += value;
    }

    public override function update(elapsed:Float):Void {
        super.update(elapsed);
        // add force to objects
        applyForceField();
        
        if(velocity.y > 0) {
            playFrontAnim();
        }
        else if(velocity.y < 0)  {
            playBackAnim();
        }
    }

    public function applyForceField():Void {
        FlxG.state.forEachOfType(bluemonkeys.AttractableObject, function(other:bluemonkeys.AttractableObject) {
            if (other.active) {
                if (force == NONE){
                    other.setForce(this, ZERO_VECTOR);
                } else {
                    if (Reg.alternativeForceModel) {
                        applyAcceleration(other);
                    } else {
                        applyVelocity(other);
                    }
                }
            }
        }, true);
    }
    private function applyAcceleration(other:AttractableObject) {
        var distance = FlxMath.distanceBetween(this, other);
        // magic function to calculate acceleration based on distance
        var acceleration = forceValue * 160.0 /(distance * distance);
        var angle = Math.atan2(this.y - other.y, this.x - other.x);
        acceleration *= (force == ATTRACT) ? 1 : -1;
        other.setForce(this, (new FlxVector(Math.cos(angle), Math.sin(angle))).scale(acceleration));
    }
    private function applyVelocity(other:AttractableObject) {
        var directionVector = new FlxVector(this.x - other.x, this.y - other.y).normalize();
        var objForce = FlxMath.distanceBetween(this, other);
        directionVector.scale(attractionMultiplier * attractionFactor/objForce);
        other.setForce(this, (force == ATTRACT) ? directionVector : directionVector.scale(-1));
    }

    public function setPolarity(polarity:ForceType) {
        force = polarity;
    }

    public function setMovementDirection(direction:FlxPoint) {
        velocity.set(direction.x, direction.y);
        velocity.scale(speed);
    }

    public function doDamage(damage:Float):Void {
        this.health -= damage;
        if (health <= 0) {
            sfxPlayer.play();
            setPolarity(NONE);
            applyForceField();
            applyDeathOnScore();
            kill();
            respawn();
        }
        FlxG.watch.addQuick("Player:doDamage",'$health');
    }

    public function applyDeathOnScore() {
        addScore(-50);
    }

    public function respawn() {
        new FlxTimer().start(3, function(timer:FlxTimer) {
            health = maxHealth;
            this.x = spawnPoint.x;
            this.y = spawnPoint.y;
            revive();
        }, 1);
    }

    public function applyHealthKit(restorePoints:Int):Void {
        health += restorePoints;
        if (health > maxHealth) {
            health = maxHealth;
        }
        FlxG.watch.addQuick("Player:applyHealthKit",'$health');
    }

    public function applySpeedModifier(speedModifier:Float, durationInSeconds:Float) {
        if(speedModifierOn) return;
        speedModifierOn = true;
        speed *= speedModifier;
        var speedTimer:FlxTimer = new FlxTimer();
        speedTimer.start(durationInSeconds, function(timer:FlxTimer) {
            speed /= speedModifier;
            speedModifierOn = false;
        }, 1);
    }

    public function applyForceModifier(forceModifier:Float, durationInSeconds:Float) {
        if(forceModifierOn) return;
        forceModifierOn = true;
        if (Reg.alternativeForceModel){
            forceValue *= forceModifier;
        } else {
            attractionMultiplier *= forceModifier;
        }
        var attractionTimer:FlxTimer = new FlxTimer();
        attractionTimer.start(durationInSeconds, function(timer:FlxTimer) {
            if (Reg.alternativeForceModel) {
                forceValue /= forceModifier;
            } else  {
                attractionMultiplier /= forceModifier;
            }
            forceModifierOn = false;
        }, 1);
    }
}